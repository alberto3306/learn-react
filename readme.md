# React

- [React](#react)
- [Next-gen JS](#next-gen-js)
  - [Variables](#variables)
  - [Arrow functions](#arrow-functions)
  - [Modules](#modules)
  - [Classes](#classes)
    - [Definition](#definition)
    - [Usage](#usage)
    - [Inheritance](#inheritance)
  - [Spread Operator](#spread-operator)
  - [Destructuring](#destructuring)
  - [Primitive and reference types](#primitive-and-reference-types)
  - [Array functions](#array-functions)
- [Development environment](#development-environment)
  - [create-react-app](#create-react-app)
  - [Development server](#development-server)
  - [Scripts](#scripts)
  - [Folders](#folders)
- [JSX](#jsx)
  - [React.createElement](#reactcreateelement)
- [Components](#components)
  - [Usage](#usage-1)
  - [Definition](#definition-1)
  - [State](#state)
- [Events](#events)
  - [Passing handlers to other components](#passing-handlers-to-other-components)
  - [Passing arguments to handlers](#passing-arguments-to-handlers)
  - [Returning values to the handlers](#returning-values-to-the-handlers)
- [Styling](#styling)
  - [Including CSS](#including-css)
  - [Inline Styles](#inline-styles)
  - [Changing CSS classes dynamically](#changing-css-classes-dynamically)
  - [Using Radium](#using-radium)
  - [CSS Modules](#css-modules)
- [Debugging](#debugging)
  - [Browser developer tools](#browser-developer-tools)
  - [React developer tools](#react-developer-tools)
  - [Error boundaries](#error-boundaries)

# Next-gen JS

## Variables

* Don't use `var`.
* Use `let` for variables that change.
* Use `const` for variables that don't change value.

## Arrow functions

* More compact in some cases.
* Arrow functions treats the `this` keyword in a more predictable way.

```
// With arguments
const myFunc = (arg1, arg2) => { return doSomething(arg1, arg2) }

// Only one argument
const myFunc = onlyOneArg => { return doSomething(onlyOneArg) }

// No arguments
const myFunc = () => { return doSomethingWithoutArgs() }

// With only one expression, it returns its value
const myFunc = (x) => doSomething(x)
```


## Modules

Exports:
```
// Default export
export default whatever

// Named export
export const whatever = 10

// Export a function
export const whatever = () => {...}
```

Imports:
```
// Import the default from a module and give it a name
import whatever from './something.js'

// Import something explicitly from a module
import { something } from './something.js'

// Import explicitly and give it another name
import { something as whatever } from './something.js'

// Import all the module exports into an object
import * as everything from './something.js'
```

## Classes

### Definition
```
class Something {

    // As if these where in the constructor (preferred)
    someProperty = 'Some Value'
    someMethod = () => {...} // No problems with the "this" keyword

    // Deprecated
    constructor() {
        this.anotherProperty = 'another value'
    }
}
```

### Usage
```
// Instantiation
const whatever = new Something()

// Calling methods
whatever.someMethod()

// Accessing properties
console.log(whatever.someProperty);
```

### Inheritance
```
class Another extends Something {

    constructor() {
        super() // Will thrown an error if absent.
    }
}
```
* When extending a class constructor it's *mandatory* to call the `super`.

## Spread Operator

With array elements:
```
arrayA = [1, 2, 3, 4]
arrayB = [...arrayA, 5, 6]
// arrayB will have arrayA's element plus its own.
```

With object properties:
```
const objectA = {
    propA = 'valueA'
};

const objectB = {
    ...objectA,
    propB = 'valueB'

    // This object will have all the properties from objectA plus its own.
}
```

With function arguments:
```
const fn = (...args) => {
    // args will be an array with all the arguments
}
```

## Destructuring

With arrays:
```
[x, y, z] = [1, 2, 3]

// x == 1
// y == 2
// z == 3

[a, , c] = [1, 2, 3]
// a == 1
// c == 3
```

With objects:
```
{propA} = {propA: 'valueA', propB: 'valueB'}

// propA == 'valueA'
// propB == undefined
```

## Primitive and reference types

Primitive types:
* numbers
* strings
* booleans

Reference types:
* objects
* arrays

Copying objects:
```
obj1 = {prop:'value'}
obj2 = {..obj1} // Will copy the properties
```

## Array functions

```
array.filter(fn)
array.map(fn)
array.find(fn)
```
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

# Development environment

## create-react-app

```
$ sudo npm install -g create-react-app
$ create-react-app <app-folder-name>
$ cd <app-folder-name>
$ npm start
```

## Development server

```
npm start
```

## Scripts

```
npm run <script>
```

## Folders

* node_modules: dependencies
* public: publicable static files
  * index.html: root file, add libraries here
  * manifest.json: app metadata
* src: compilable source code
  * index.js: main app file
  * App.css vs. index.css ??
  * App.tests.js: unit testing

# JSX

* Is not HTML, is javascript made to resemble it
* cannot use `class`, use `className`

JS code is run between braces:
```
Hello {name}!
Today is {Date.now()}.
```
  
## React.createElement

```
React.createElement(tag, configuration, ...children)

// Children can be more createElement calls
// Configuration is an object with the tag attributes
// ex:
//  {
//      className: 'some-class';
//  }
```

# Components

## Usage

```
import OtherComponent from './OtherComponent';

// Inside JSX
<Othercomponent/>

// With attributes
<Othercomponent attr1="one" attr2="two"/>

// With attributes and children
<OtherComponent attr1="one">
    // Children
    Some content.
    <AnotherComponent/>
</OtherComponent>
```

## Definition

```
import React, { Component } from 'react';

// Must extend Component
class TheComponent extends Component {

    // Has to render something
    render() {
        <h1>some JSX code</h1>
        <p>Hello, { this.props.name }</p>
        <p>{ this.props.children }</p>
    }
}
```

Components can also be functions (stateless):
```
const theComponent = (props) => {
    return (
        <h1>some JSX code</h1>
        <p>Hello, { props.name }.</p>
        <p>{ this.props.children }</p>
    )
}
```

* `App` is the main root component.
* The exported name should be the same as the file.
* The exported name should be capitalized.
* The file should import React to JSX be available.

## State

 * Components can have properties.
 * `state` is a reserved property name.
 * Changes in `state`'s members trigger DOM updates.
 * The state can be changed from outside the component.

```
class ComponentWithState extends Component {

    state = {
        name: "Person"
    }

    render () {
        <h1>Hello, my name is { this.state.name }.</h1>
    }
}
```

# Events

 * We can call component methods when events are triggered.
 * React can listen to these events: https://reactjs.org/docs/events.html#supported-events
 * The capitalization of event names is important.
 * Use `setState` to manipulate the state.

```
class TheComponent extends component {

    state = {
        clicks: 0
    }

    theEventHandler = () => {
        console.log('the event happened');
        // Don't manipulate this.state directly!
        // this.state.clicks++
        this.setState({ clicks: this.state.clicks + 1 })
    }

    render () {
        <button onClick={ this.theEventHandler }>Click me!</button>
        <p>
    }
}
```

## Passing handlers to other components

Pass the handler method as a prop:
```
class TheComponent extends Component {

    const buttonClickHandler = () => {
        console.log('A button was clicked.")
    }

    render () {
        <CustomButton clickHandler={ this.buttonClickHandler } />
    }
}
```

Use the received handler:
```
const customButton = props => {
    <button onClick={ props.clickHandler }>Click me!</button>
}
```

## Passing arguments to handlers

Binding the method to the current component:
```
<OtherComponent handler={ this.theHandler.bind(this, some, arguments) } />
```
 * When called, the handler's `this` will be the binded to the main component.

Using an closure:
```
<OtherComponent handler={ () => this.theHandler() } />
```
 * Inefficient

## Returning values to the handlers

Via event:

```
const eventHandler = (event) => {
    console.log(event.target.value)
}
```

# Styling

## Including CSS

* The CSS code is _global_. We must prefix the HTML and CSS rules ourselves.
* Use `className` instead of `class`

```
import "style.css"

<button className="theButton" />
```

## Inline Styles

 * Declare the style as a JS object.
 * Use it in JSX with the `style` attribute.
 * The CSS properties are camel cased.

```
class TheComponent extends Component {

    const style = {
        backgroundColor: 'blue',
        border: '3px solid gray'
    }

    render () {

        // We can manipulate the style depending on other properties or state
        style = {...this.style}
        style.color = this.state.disabled ? 'grey' : 'black'

        return (
            <button style={style} />
        )
    }
}
```

## Changing CSS classes dynamically

```
    const classes = ['one', 'two']

    // Add class conditionally
    if (this.state.three) {
        classes.push('three')
    }

    // Assign the class names
    return (
        <div className={classes.join(' ')}>Text</div>
    )
```

## Using Radium

 * Allow usage of CSS pseudo-selectors
 * Allow usage of media queries if the _App_ is wrapped in StyleRoot
 * `npm install --save radium`
 * Must wrap the exported component or App

```
import React, { Component } from 'react';
import Radium, { StyleRoot } from 'radium';

class App extends Component {

    render () {

        // Use a string index to specify pseudo-selectors
        const style = {
            color: 'black',
            ':hover' : {
                color: 'red',
                font-weight: 'bold'
            },
            '@media (min-width: 500px)': {
                border-color: 'red'
            }
        }

        // Access the style using square brackets
        if (parms.big) {
            style[':hover'].font-size: 'xl'
        }

        return (
            // Wrapping for media queries
            <StyleRoot>
                <button style={style}/>
            <StyleRoot/>
        )
    }

}

// Wrap the exported component in Radium
export default Radium(App)
```

## CSS Modules

Allows applying CSS classes directly to JSX without applying them to all the generated HTML.
Also pseudo-selectors and media queries.

 * Eject the project: `npm run eject`
 * Activate the modules in `config/webpack.config.js`
 * _restart the dev server_
 * Import classes from the CSS
 * The classes are still strings, but in variables
 * Use the desired class names in className attributes

Webpack config:
```
    {
      test: cssRegex,
      exclude: cssModuleRegex,
      use: getStyleLoaders({
          importLoaders: 1,
          modules: true,
          localIdentName: '[name]__[local]__[hash:base64:5]'
      }),
    }
```

Usage:
```
import classes from './App.css';

const theComponent = () => {

    // Classes are variables containing generated internal CSS class names
    const theClasses = [classes.one, classes.two]
    
    // This is still possible
    theClasses.push(classes.three)

    // The classes are strings, so we need to join them
    <button className={theClasses.join(' ')}/>
}
```

# Debugging

## Browser developer tools

 * Source maps help to see the original code instead of the compilation.

## React developer tools

 Downloads:
  * [Standalone](https://github.com/facebook/react-devtools)
  * [Firefox](https://addons.mozilla.org/en-US/firefox/addon/react-devtools/)
  * [Chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
  
  A new tab will appear in the browser developer tools, showing the component structure and state.

## Error boundaries

 * Standard React component implementing the `componentDidCatch` method.

```
class ErrorBoundary extends Component {

    componentDidCatch(error, info) {
        // Do something with the error information.
        // Save it to the state, log it, send it to logging facility... etc.
    }

    render() {

        if (this.state.hasError) {
            // Show only the error.  
            return <h1>Error message</h1>
        }

        // Render the wrapped components.
        return this.props.children;
    }
}
```

Wrap the components from which we want to catch errors:

```
class TheComponent extends Component {

    render() {
        <ErrorBoundary key={ this.state.key }>
            <SomeOtherComponent/>
        </ErrorBoundaries>
    }
}
```
