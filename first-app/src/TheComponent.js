import React, { Component } from 'react';
import OtherComponent from './OtherComponent'

class TheComponent extends Component {

    state = {
        others: [
            { key: 'a', value: 'one' },
            { key: 'b', value: 'two' },
            { key: 'c', value: 'three' },
        ]
    }

    render() {
        return (
            <ol>
                {this.state.others.map(x => {
                    return <OtherComponent key={x.key} value={x.value} />
                })}
            </ol>
        )
    }
}

export default TheComponent