import React, { Component } from 'react';
import TheComponent from './TheComponent'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Hello.</h1>

        <TheComponent />

      </div>
    );
  }
}

export default App;
