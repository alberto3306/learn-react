import React, { Component } from 'react';
import ToggleButton from './ToggleButton';

class OtherComponent extends Component {

    state = {
        showIt: true
    }

    render() {
        return (
            <li>
                <ToggleButton toggleCallback={this.toggleShowIt} on={this.state.showIt} />
                {this.state.showIt ? this.renderValue() : null}
            </li >
        )
    }

    toggleShowIt = () => {
        this.setState({ showIt: !this.state.showIt })
    }

    renderValue() {
        return (
            <span>{this.props.value}</span>
        )
    }
}

export default OtherComponent