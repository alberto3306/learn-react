import React from 'react'

const toggleButton = (props) => {

    const label = props.on ? "Hide" : "Show"

    return (
        <button onClick={props.toggleCallback}> {label}</button>
    )
}

export default toggleButton;